import { AzureFunction, Context, HttpRequest } from '@azure/functions';
import { notFound, badRequest } from '@hapi/boom';
import mongoConnect from '../shared/libs/connectToMongo';
import { UserModel } from '../shared/models/User';
import requestPipeline from '../shared/libs/requestPipeline';
import { generateResponse } from '../shared/libs/responseHelpers';
import { isValidObjectId } from '../shared/libs/utils';
import mongoQueryGenerator from '../shared/libs/middlewares/mongoQueryGenerator';

const httpTrigger: AzureFunction = async (context: Context, req: HttpRequest): Promise<void> => {
  await mongoConnect();

  const { id: _id } = req.params;
  if (!isValidObjectId(_id)) throw badRequest('ID do usuário inválido!');

  const { population } = req.mongoQuery;

  const user = await UserModel.findOne({ _id })
    .select('-password')
    // .populate(population);

  if (user) {
    user.password = undefined;

    // console.log(user);
    context.res = generateResponse(user, 200);
  } else {
    throw notFound('Usuário não encontrado!');
  }
};

export default requestPipeline(mongoQueryGenerator, httpTrigger);
