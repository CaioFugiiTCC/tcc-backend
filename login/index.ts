import { AzureFunction, Context, HttpRequest } from "@azure/functions"
import connectToMongo from "../shared/libs/connectToMongo";
import { unauthorized, badRequest } from "@hapi/boom"
import { generateResponse } from "../shared/libs/responseHelpers";
import { UserModel } from '../shared/models/User';
import { UserDataJwt, createJwt } from "../shared/libs/tokenJwt";


const getUserByUsername = (username: string) => UserModel.findOne({ username });

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
  await connectToMongo();

  if (!req.body || !req.body.username || !req.body.password) {
    throw badRequest('É necessário informar usuário e senha.');
  }

  const { username, password } = req.body;


  const user = await getUserByUsername(username);


  if (!user) {
    throw unauthorized('Usuário ou senha inválidos.');
  }

  const userData: UserDataJwt = {
    _id: user._id,
    isActive: user.isActive,
    username: user.username,
    name: user.name,
    email: user.email,
    roles: user.roles,
    bankAccount: user.bankAccount
  };
  const token = createJwt(userData);

  context.res = generateResponse({ data: token });

};


export default httpTrigger;
