import { AzureFunction, Context, HttpRequest } from '@azure/functions';
import { notFound } from '@hapi/boom';
import mongoConnect from '../shared/libs/connectToMongo';
import { UserModel } from '../shared/models/User';
import requestPipeline from '../shared/libs/requestPipeline';
import { generateResponse } from '../shared/libs/responseHelpers';
import mongoQueryGenerator from '../shared/libs/middlewares/mongoQueryGenerator';

const httpTrigger: AzureFunction = async (context: Context, req: HttpRequest): Promise<void> => {
  await mongoConnect();

  const {
    filter,
    skip,
    limit,
    sort,
    population,
  }: any = req.mongoQuery;

  if (filter) {
    if (filter.code) {
      delete filter.code;
    }
    if (filter.name) {
      filter.name = { $regex: `^${filter.name}`, $options: 'i' };
    }
  }

  const users = await UserModel.find(filter)
    // .populate(population)
    .sort(sort)
    .skip(skip)
    .limit(limit)
    .select('-password');

  const userCount = await UserModel.count(filter);

  if (users.length) {
    context.res = generateResponse(users, 200, {
      'x-total-count': userCount,
      'Access-Control-Expose-Headers': 'x-total-count',
    });
  } else {
    throw notFound('Nenhum usuário encontrado!');
  }
};

export default requestPipeline(mongoQueryGenerator, httpTrigger);
