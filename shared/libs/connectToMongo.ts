import * as mongoose from 'mongoose'

// const database = 'refundme-db'

export default async function connectToMongo(
  // connectionString: string = process.env.MongoConnectionString,
  connectionString: string = "mongodb://refundme-db:5lG8nUYu6QGuS6vN7MPmx06WYdLSIAR55YkBH6DiykNIB1bsUy5csKabd34vOPJO1mvalUwuWzvC7mKCk6VRxg==@refundme-db.documents.azure.com:10255/refundme-db?ssl=true&replicaSet=globaldb",
  autoIndex: boolean = true
) {

  // console.log(connectionString);
  if (!mongoose.connection || !mongoose.connection.readyState) {
    await mongoose.connect(connectionString, {
      autoIndex,
      useCreateIndex: true,
      useNewUrlParser: true,
      useFindAndModify: false
    });
  }

  return mongoose.connection;
}
