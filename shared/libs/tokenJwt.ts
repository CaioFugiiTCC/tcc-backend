import * as jwt from 'jsonwebtoken';

interface UserJwt {
  _id: string;
}

export interface UserDataJwt {
  _id: string;
  isActive: boolean;
  username: string;
  name: string;
  email: string;
  roles: string[];
  bankAccount: any
}

const tokenValidation = (data: UserJwt, expiration: number | string = '1d') =>
  jwt.sign({ data }, process.env.SECRET, { expiresIn: expiration });

const createJwt = (user: UserDataJwt, expiration: number | string = '1d') =>
  jwt.sign({ user }, process.env.SECRET, { expiresIn: expiration });

export { createJwt, tokenValidation };
