import {
  prop, Typegoose, Ref, arrayProp, instanceMethod,
} from 'typegoose';
import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';

class UserRole {
  system: string;

}

class bankAccount {
  @prop({ required: false })
  bankName: string;

  @prop({ required: false })
  agency: string;

  @prop({ required: false })
  account: string;
}

class User extends Typegoose {
  @prop({ required: true })
  name: string;

  @prop({ required: true, unique: true })
  username: string;

  @prop({ required: true, unique: true, validate: /\S+@\S+\.\S+/ })
  email: string;

  @prop({ required: false })
  password: string;

  @prop({ default: true })
  isActive: boolean;

  @prop({ required: false })
  bankAccount: bankAccount;

  @arrayProp({ items: UserRole, default: [] })
  roles: Ref<UserRole>[];

  @instanceMethod
  // eslint-disable-next-line class-methods-use-this
  async setPassword(password: string) {
    try {
      const salt = await bcrypt.genSalt(10);
      return bcrypt.hash(password, salt);
    } catch (error) {
      throw new Error(error);
    }
  }

  @instanceMethod
  async isPasswordValid(password: string) {
    try {
      return bcrypt.compare(password, this.password);
    } catch (error) {
      throw new Error(error);
    }
  }

}

const UserModel = new User().getModelForClass(User, {
  existingMongoose: mongoose,
  schemaOptions: { collection: 'users', timestamps: true },
});

export { User, UserModel };
